package com.bss.media.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import org.mindrot.jbcrypt.BCrypt;

import com.bss.media.shared.entity.AuthInfo;
import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.BSSStatus;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.googlecode.objectify.ObjectifyService;

public class InitDataGenerator {
    
    public InitDataGenerator() {
        ObjectifyService.factory().register(BSSMember.class);
        ObjectifyService.factory().register(AuthInfo.class);
    }
    
    public void generateStudioMembers() {
        BSSMember member = new BSSMember("Árendás Csaba", "Csabi", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Szadami Máté", "Máté", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Dévai Dániel", "DDani", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Gárdonyi Ágnes Anna", "Ági", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Grób Csaba", "Csaba", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Hegedűs Csaba", "Csabi", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Kiss Dávid", "Dave", BSSStatus.STUDIOS);
        ofy().save().entities(member);
        member = new BSSMember("Nemes Dávid", "Nemes", BSSStatus.STUDIOS);
        ofy().save().entities(member);
    }
    
    public void generateInitAuthInfo() {
        String userName = "";
        String password = "";
        String hash = BCrypt.hashpw(password, BCrypt.gensalt(12));
        AuthInfo user = new AuthInfo(userName, hash, false);
        ofy().save().entity(user).now();
        
        String adminName = "";
        String adminPassword = "";
        String adminHash = BCrypt.hashpw(adminPassword, BCrypt.gensalt(12));
        AuthInfo admin = new AuthInfo(adminName, adminHash, true);
        ofy().save().entity(admin).now();
        
        LTOTapeEntity lTOTapeEntity = new LTOTapeEntity("LTO_01", 300);
        ofy().save().entity(lTOTapeEntity).now();
    }
}
