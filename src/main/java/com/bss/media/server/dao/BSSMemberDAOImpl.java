package com.bss.media.server.dao;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.MediaEntity;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Result;


public class BSSMemberDAOImpl implements BSSMemberDAO {
    
    public BSSMemberDAOImpl() {
        ObjectifyService.factory().register(MediaEntity.class);
        ObjectifyService.factory().register(BSSMember.class);
    }

    @Override
    public List<BSSMember> getAll() {
        return ofy().load().type(BSSMember.class).list();
    }

    @Override
    public Result<Key<BSSMember>> save(BSSMember entity) {
        return ofy().save().entity(entity);
    }
    
}
