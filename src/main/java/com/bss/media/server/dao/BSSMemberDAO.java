package com.bss.media.server.dao;

import java.util.List;

import com.bss.media.shared.entity.BSSMember;
import com.google.inject.ImplementedBy;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Result;

@ImplementedBy(BSSMemberDAOImpl.class)
public interface BSSMemberDAO {
    
    Result<Key<BSSMember>> save(BSSMember entity);
    
    List<BSSMember> getAll();
}
