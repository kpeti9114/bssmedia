package com.bss.media.server.dao;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import com.bss.media.shared.entity.AuthInfo;
import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.bss.media.shared.entity.MediaEntity;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Result;


public class MediaDAOImpl implements MediaDAO {
    
    public MediaDAOImpl() {
        ObjectifyService.factory().register(MediaEntity.class);
        ObjectifyService.factory().register(BSSMember.class);
        ObjectifyService.factory().register(AuthInfo.class);
        ObjectifyService.factory().register(LTOTapeEntity.class);
    }
    
    @Override
    public Key<MediaEntity> save(MediaEntity entity) {
        return ofy().save().entity(entity).now();
    }

    @Override
    public Result<Void> delete(MediaEntity entity) {
        return ofy().delete().entity(entity);
    }

    @Override
    public MediaEntity get(Long id) {
        return ofy().load().type(MediaEntity.class).id(id).now();
    }
    
    @Override
    public List<MediaEntity> search(List<String> searchValues) {
        List<MediaEntity> result = new ArrayList<>();
        List<MediaEntity> entities = getAll();
        for (MediaEntity mediaEntity : entities) {
            if(mediaEntity.matchWithSearchValues(searchValues)) {
                result.add(mediaEntity);
            }
        }
        return result;
    }

    @Override
    public List<MediaEntity> getAll() {
        return ofy().load().type(MediaEntity.class).list();
    }

    @Override
    public List<AuthInfo> getAllAuthInfo() {
        return ofy().load().type(AuthInfo.class).list();
    }

    @Override
    public LTOTapeEntity getLTOTape() {
        return ofy().load().type(LTOTapeEntity.class).list().get(0);
    }

    @Override
    public Key<LTOTapeEntity> updateLTOInfo(LTOTapeEntity entity) {
        return ofy().save().entity(entity).now();
    }
    
}
