package com.bss.media.server.dao;

import java.util.List;

import com.bss.media.shared.entity.AuthInfo;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.bss.media.shared.entity.MediaEntity;
import com.google.inject.ImplementedBy;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Result;


@ImplementedBy(MediaDAOImpl.class)
public interface MediaDAO {
    
    Key<MediaEntity> save(MediaEntity entity);
    
    Result<Void> delete(MediaEntity entity);

    MediaEntity get(Long id);
    
    List<MediaEntity> search(List<String> searchValues);

    List<MediaEntity> getAll();
    
    List<AuthInfo> getAllAuthInfo();
    
    LTOTapeEntity getLTOTape();
    
    Key<LTOTapeEntity> updateLTOInfo(LTOTapeEntity entity);
}
