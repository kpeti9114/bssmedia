package com.bss.media.server;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.mindrot.jbcrypt.BCrypt;

import com.bss.media.client.MediaArchiveService;
import com.bss.media.server.dao.BSSMemberDAO;
import com.bss.media.server.dao.BSSMemberDAOImpl;
import com.bss.media.server.dao.MediaDAO;
import com.bss.media.server.dao.MediaDAOImpl;
import com.bss.media.shared.entity.AuthInfo;
import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.bss.media.shared.entity.MediaEntity;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.google.gwt.thirdparty.guava.common.collect.Lists;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class MediaArchiveServiceImpl extends RemoteServiceServlet implements MediaArchiveService {
    
    private InitDataGenerator dataGenerator = new InitDataGenerator();
    
    private MediaDAO mediaDAO = new MediaDAOImpl();
    
    private BSSMemberDAO memberDAO = new BSSMemberDAOImpl();
    
    @Override
    public Long saveMediaEntity(MediaEntity entity) {
        return mediaDAO.save(entity).getId();
    }
    
    @Override
    public MediaEntity getEntity(Long id) {
        return mediaDAO.get(id);
    }
    
    @Override
    public List<MediaEntity> searchEntity(List<String> searchValues) {
        return mediaDAO.search(searchValues);
    }
    
    @Override
    public List<BSSMember> getMembers() {
        return Lists.newArrayList(memberDAO.getAll());
    }
    
    @Override
    public void createInitData() {
        List<AuthInfo> listOfAuthInfo = mediaDAO.getAllAuthInfo();
        if (listOfAuthInfo.isEmpty()) {
            dataGenerator.generateStudioMembers();
            dataGenerator.generateInitAuthInfo();
        }
    }
    
    @Override
    public boolean authenticateUser(String userName, String password) {
        List<AuthInfo> listOfAuthInfo = mediaDAO.getAllAuthInfo();
        for (AuthInfo authInfo : listOfAuthInfo) {
            if (authInfo.getUserName().equals(userName)) {
                String hashedPassword = authInfo.getHashedPassword();
                return BCrypt.checkpw(password, hashedPassword);
            }
        }
        return false;
    }
    
    @Override
    public boolean isUserAdmin(String userName, String password) {
        List<AuthInfo> listOfAuthInfo = mediaDAO.getAllAuthInfo();
        for (AuthInfo authInfo : listOfAuthInfo) {
            if (authInfo.getUserName().equals(userName)) {
                String hashedPassword = authInfo.getHashedPassword();
                if (BCrypt.checkpw(password, hashedPassword)) {
                    return authInfo.getIsAdmin();
                }
            }
        }
        return false;
    }
    
    @Override
    public List<String> getVideos() {
        List<String> result = new ArrayList<>();
        URLFetchService urlFetchService = URLFetchServiceFactory.getURLFetchService();
        URL url;
        try {
            url = new URL("http://xx:8080/media-archive/ListingServlet");
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unhandled exception!", e);
        }
        HTTPRequest httpRequest = new HTTPRequest(url, HTTPMethod.GET);
        HTTPResponse response;
        try {
            response = urlFetchService.fetch(httpRequest);
        } catch (IOException e) {
            throw new RuntimeException("Unhandled exception!", e);
        }
        String content = new String(response.getContent());
        String[] contentTable = content.split("<hr></th></tr>");
        String[] lines = contentTable[1].split("</td></tr>");
        for (int j = 1; j < lines.length; j++) {
            String[] lineElements = lines[j].split("</td>");
            for (int i = 0; i < lineElements.length; i++) {
                String lineElement = lineElements[i];
                if (lineElement.contains("<a href")) {
                    String[] resultArray = lineElement.split("\">");
                    result.add(resultArray[1].substring(0, resultArray[1].length() - 4));
                    resultArray = lineElements[i+1].split(">");
                    result.add(resultArray[1]);
                    resultArray = lineElements[i+2].split(">");
                    result.add(resultArray[1]);
                }
            }
        }
        return result;
    }

    @Override
    public LTOTapeEntity getLTOInfo() {
        return mediaDAO.getLTOTape();
    }

    @Override
    public void updateLTOInfo(LTOTapeEntity entity) {
        mediaDAO.updateLTOInfo(entity);
    }

    @Override
    public boolean isLTOWritable() {
        return mediaDAO.getLTOTape().isWritable();
    }

    @Override
    public void setLTOWritableInfo(boolean isWritable) {
        LTOTapeEntity entity = mediaDAO.getLTOTape();
        entity.setWritable(isWritable);
        mediaDAO.updateLTOInfo(entity);
    }

    @Override
    public void sendEmailToAdministrator(MediaEntity entity) {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        String msgBody = "Archiválást kezdeményeztek a(z)"
            + " " + entity.getLtoName()
            + " nevű LTO kazettára. Az archiválandó fájl neve: "
            + entity.getName() + " Ne felejtse el engedélyezni az archiválási lehetőséget"
            + " a sikeres archiválás folyamat végeztével!";

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("admin@bss_media.com", "bss_media Admin"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress("kpeti9114@gmail.com", "Peter Kismarczi"));
            msg.setSubject("Archiválási kezdeményezés");
            msg.setText(msgBody);
            Transport.send(msg);

        } catch (AddressException e) {
            throw new RuntimeException("E-mail send exception: " + e.getMessage());
        } catch (MessagingException e) {
            throw new RuntimeException("E-mail send exception: " + e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("E-mail send exception: " + e.getMessage());
        }
    }
    
}
