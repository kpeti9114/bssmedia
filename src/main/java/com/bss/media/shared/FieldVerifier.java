package com.bss.media.shared;


public class FieldVerifier {

  public static boolean isValidDate(String date) {
      return date.matches("((18|19|20)\\d{2})-([1-9]|0[1-9]|1[0-2])-(0[1-9]|[1-9]|[12][0-9]|3[01])");
  }
  
  public static boolean isFilledText(String text) {
      if (text.isEmpty() || text.equals("") || text.equals(" ")) {
        return false;
    } else {
        return true;
    }
  }
  
}
