package com.bss.media.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class MediaEntity implements Serializable {
    
    @Id
    private Long id;
    
    private String name;
    
    private String title;
    private Date date;
    private String event;
    private String description;
    private Map<BSSPost, ArrayList<String>> staff = new HashMap<BSSPost, ArrayList<String>>();
    
    private boolean isDeletable;
    
    private String ltoName;
    private String folderName;
    
    public MediaEntity() {}
    
    public boolean matchWithSearchValues(List<String> searchValues) {
        if ( !searchValues.get(0).isEmpty() ) {
            if( !this.name.toLowerCase().contains(searchValues.get(0).toLowerCase()) ) {
                return false;
            }
        }
        if ( !searchValues.get(1).isEmpty() ) {
            if( !this.title.toLowerCase().contains(searchValues.get(1).toLowerCase()) ) {
                return false;
            }
        }
        if ( !searchValues.get(2).isEmpty() ) {
            String[] searchValueString = searchValues.get(2).split("-");
            String[] dateString = this.date.toString().split(" ");
            if( !searchValueString[0].equals(dateString[5]) ) {
                return false;
            }
            if( !searchValueString[1].equals("**") ) {
                int month = Integer.parseInt(searchValueString[1]);
                if( !(month != this.date.getMonth()-1) ) {
                    return false;
                }
            }
        }
        if ( !searchValues.get(3).isEmpty() ) {
            if( !this.event.toLowerCase().contains(searchValues.get(3).toLowerCase()) ) {
                return false;
            }
        }
        if ( !searchValues.get(4).isEmpty() ) {
            if( !this.description.toLowerCase().contains(searchValues.get(3).toLowerCase()) ) {
                return false;
            }
        }
        if ( !searchValues.get(5).isEmpty() && !searchValues.get(6).isEmpty()) {
            if(this.staff.containsKey(searchValues.get(5))) {
                if ( !this.staff.get(searchValues.get(5)).contains(searchValues.get(6)) ) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getEvent() {
        return event;
    }
    
    public void setEvent(String event) {
        this.event = event;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Map<BSSPost, ArrayList<String>> getStaff() {
        return staff;
    }
    
    public void setStaff(Map<BSSPost, ArrayList<String>> staff) {
        this.staff = staff;
    }
    
    public boolean isDeletable() {
        return isDeletable;
    }
    
    public void setDeletable(boolean isDeletable) {
        this.isDeletable = isDeletable;
    }
    
    public String getLtoName() {
        return ltoName;
    }
    
    public void setLtoName(String ltoName) {
        this.ltoName = ltoName;
    }
    
    public String getFolderName() {
        return folderName;
    }
    
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }
    
}
