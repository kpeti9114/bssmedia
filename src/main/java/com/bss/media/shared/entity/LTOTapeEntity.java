package com.bss.media.shared.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class LTOTapeEntity implements Serializable {
    
    @Id
    private Long id;
    
    private String ltoName;
    private long freeSpace;
    private boolean isWritable;

    public LTOTapeEntity() {}
    
    public LTOTapeEntity(String ltoName, long freeSpace) {
        this.ltoName = ltoName;
        this.freeSpace = freeSpace;
        this.isWritable = true;
    }
    
    public String getLtoName() {
        return ltoName;
    }
    public void setLtoName(String ltoName) {
        this.ltoName = ltoName;
    }
    public long getFreeSpace() {
        return freeSpace;
    }
    public void setFreeSpace(long freeSpace) {
        this.freeSpace = freeSpace;
    }
    
    public boolean isWritable() {
        return isWritable;
    }

    public void setWritable(boolean isWritable) {
        this.isWritable = isWritable;
    }
    
}
