package com.bss.media.shared.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class BSSMember implements Serializable {
    
    @Id
    private Long id;
    
    private String name;
    private String nickname;
    private BSSStatus status;
    
    private String actualPost;

    public BSSMember() {}
    
    public BSSMember(String name, String nickname, BSSStatus status) {
        this.name = name;
        this.nickname = nickname;
        this.status = status;
    }
    
    public BSSMember(String name, String actualPost) {
        this.name = name;
        this.actualPost = actualPost;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public String getListName() {
        return this.name + " (" + this.nickname + ")";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public BSSStatus getStatus() {
        return status;
    }

    public void setStatus(BSSStatus status) {
        this.status = status;
    }
    
    public String getActualPost() {
        return actualPost;
    }

    public void setActualPost(String actualPost) {
        this.actualPost = actualPost;
    }
    
}
