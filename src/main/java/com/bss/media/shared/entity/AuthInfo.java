package com.bss.media.shared.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Cache
@Entity
public class AuthInfo implements Serializable{
    
    @Id
    private Long id;
    
    private String userName;
    private String hashedPassword;
    private boolean isAdmin;

    public AuthInfo() {}
    
    public AuthInfo(String userName, String hashedPassword, boolean isAdmin) {
        this.userName = userName;
        this.hashedPassword = hashedPassword;
        this.isAdmin = isAdmin;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getHashedPassword() {
        return hashedPassword;
    }
    
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
    
    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
