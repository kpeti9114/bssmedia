package com.bss.media.client;

import java.util.List;

import com.bss.media.client.ui.AdminDialogBox;
import com.bss.media.client.ui.ArchiveUI;
import com.bss.media.shared.entity.BSSPost;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class BSSMedia implements EntryPoint {
    
    private final ClientFactory clientFactory = GWT.create(ClientFactory.class);
    
    private final MediaArchiveServiceAsync mediaService = GWT.create(MediaArchiveService.class);
    
    public void onModuleLoad() {
        mediaService.createInitData(new AsyncCallback<Void>() {
            @Override
            public void onSuccess(Void result) {
                LoginDialog loginDialog = new LoginDialog();
                loginDialog.center();
                loginDialog.show();
            }
            @Override
            public void onFailure(Throwable caught) {}
        });
    }
    
    private void initUI() {
        
        final ArchiveUI archiveUI = clientFactory.getArchiveUI();
        
        List<Widget> elements = archiveUI.getElements();
        
        RootPanel.get("videoListContainer").add(elements.get(0));
        RootPanel.get("videoChooseContainer").add(elements.get(1));
        RootPanel.get("videoChooseContainer").add(elements.get(2));
        
        RootPanel.get("titleContainer").add(elements.get(3));
        RootPanel.get("titleContainer").add(elements.get(4));
        
        RootPanel.get("dateContainer").add(elements.get(5));
        RootPanel.get("dateContainer").add(elements.get(6));
        
        RootPanel.get("eventContainer").add(elements.get(7));
        RootPanel.get("eventContainer").add(elements.get(8));
        
        RootPanel.get("descriptionContainer").add(elements.get(9));
        RootPanel.get("descriptionContainer").add(elements.get(10));
        
        RootPanel.get("staffContainer").add(elements.get(11));
        RootPanel.get("staffContainer").add(elements.get(12));
        RootPanel.get("staffContainer").add(elements.get(13));
        RootPanel.get("staffContainer").add(elements.get(14));
        RootPanel.get("staffContainer").add(elements.get(15));
        RootPanel.get("staffContainer").add(elements.get(16));
        
        RootPanel.get("deleteContainer").add(elements.get(17));
        RootPanel.get("deleteContainer").add(elements.get(18));
        RootPanel.get("deleteContainer").add(elements.get(19));
        
        RootPanel.get("archiveButtonContainer").add(elements.get(20));
        RootPanel.get("archiveButtonContainer").add(elements.get(21));
        RootPanel.get("searchContainer").add(elements.get(22));
        
    }
    
    public static void addPostsToListBox(ListBox listBox) {
        for (BSSPost bssPost : BSSPost.values()) {
            listBox.addItem(bssPost.toString());
        }
    }
    
    private class LoginDialog extends DialogBox {
        
        TextBox userNameField = new TextBox();
        PasswordTextBox passwordField = new PasswordTextBox();
        
        Button loginButton = new Button("Belépés");
        
        private LoginDialog() {
            
            setText("Bejelentkezés");
            setAnimationEnabled(true);
            setGlassEnabled(true);
            
            VerticalPanel panel = new VerticalPanel();
            panel.setHeight("50");
            panel.setWidth("150");
            panel.setSpacing(5);
            panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
            
            panel.add(new Label("Felhasználónév: "));
            panel.add(userNameField);
            panel.add(new Label("Jelszó: "));
            panel.add(passwordField);
            
            loginButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    mediaService.authenticateUser(userNameField.getText(), passwordField.getText(),
                        new AsyncCallback<Boolean>() {
                            
                            @Override
                            public void onSuccess(Boolean isValidUser) {
                                if (isValidUser) {
                                    mediaService.isUserAdmin(userNameField.getText(), 
                                            passwordField.getText(), new AsyncCallback<Boolean>() {
                                                
                                                @Override
                                                public void onSuccess(Boolean result) {
                                                    if(result) {
                                                        AdminDialogBox adminDialogBox = new AdminDialogBox();
                                                        adminDialogBox.center();
                                                        adminDialogBox.show();
                                                        hide();
                                                    } else {
                                                        initUI();
                                                        hide();
                                                    }
                                                }
                                                @Override
                                                public void onFailure(Throwable caught) {}
                                            });
                                } else {
                                    Window.alert("Hibás felhasználónév vagy jelszó!");
                                }
                            }
                            @Override
                            public void onFailure(Throwable caught) {}
                        });
                }
            });
            
            panel.add(loginButton);
            
            setWidget(panel);
        }
    }
}
