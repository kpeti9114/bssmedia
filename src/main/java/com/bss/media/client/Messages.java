package com.bss.media.client;

public interface Messages extends com.google.gwt.i18n.client.Messages {
  
  @DefaultMessage("Válassza ki az archiválandó videót a listábó: ")
  String chooseLabel();
    
  @DefaultMessage("* Adja meg a video címét: ")
  String titleLabel();
  
  @DefaultMessage("* Adja meg a dátumot \"ÉÉÉÉ-HH-NN\" alakban: ")
  String dateLabel();
  
  @DefaultMessage("* Adja meg az esemény leírását, amihez a video kapcsolódik: ")
  String eventLabel();
  
  @DefaultMessage("Adjon meg a leírást: ")
  String descriptionLabel();
  
  @DefaultMessage("Válasszon posztot: ")
  String postLabel();
  
  @DefaultMessage("Válasszon tagot az adott posztra: ")
  String memberLabel();
  
  @DefaultMessage("Hozzáadás")
  String addMemberButtonLabel();
  
  @DefaultMessage("* Automatikusan törölhető?")
  String selectLabel();
  
  @DefaultMessage("Archiválás")
  String archiveButtonLabel();
  
  @DefaultMessage("A *-al jelölt mezők kitöltése kötelező!")
  String requiredLabel();
  
  @DefaultMessage("Keresés")
  String searchButtonLabel();
  
  @DefaultMessage("Mégsem")
  String cancelButtonLabel();
  
}
