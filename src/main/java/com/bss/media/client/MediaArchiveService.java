package com.bss.media.client;

import java.util.List;

import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.bss.media.shared.entity.MediaEntity;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("greet")
public interface MediaArchiveService extends RemoteService {
    
    void createInitData();
    
    boolean authenticateUser(String userName, String password);
    
    boolean isUserAdmin(String userName, String password);
    
    Long saveMediaEntity(MediaEntity entity);
    
    MediaEntity getEntity(Long id);
    
    List<MediaEntity> searchEntity(List<String> searchValues);
    
    List<BSSMember> getMembers();
    
    List<String> getVideos();
    
    LTOTapeEntity getLTOInfo();
    
    void updateLTOInfo(LTOTapeEntity entity);
    
    boolean isLTOWritable();
    
    void setLTOWritableInfo(boolean isWritable);
    
    void sendEmailToAdministrator(MediaEntity entity);
    
}
