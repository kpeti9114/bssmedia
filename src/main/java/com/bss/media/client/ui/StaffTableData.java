package com.bss.media.client.ui;

import java.util.List;

import com.bss.media.shared.entity.BSSPost;


public class StaffTableData {
    
    private BSSPost bssPost;
    private List<String> members;
    
    public StaffTableData(BSSPost bssPost, List<String> members) {
        this.bssPost = bssPost;
        this.members = members;
    }
    
    public String getMemberNames() {
        StringBuilder builder = new StringBuilder();
        for (String member : members) {
            builder.append(member + ", ");
        }
        return builder.toString();
    }
    
    public BSSPost getBssPost() {
        return bssPost;
    }
    public void setBssPost(BSSPost bssPost) {
        this.bssPost = bssPost;
    }
    public List<String> getMembers() {
        return members;
    }
    public void setMembers(List<String> members) {
        this.members = members;
    }
}
