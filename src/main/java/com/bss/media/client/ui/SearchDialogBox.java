package com.bss.media.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.bss.media.client.BSSMedia;
import com.bss.media.client.MediaArchiveService;
import com.bss.media.client.MediaArchiveServiceAsync;
import com.bss.media.client.Messages;
import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.MediaEntity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SearchDialogBox extends DialogBox {
    
    private final MediaArchiveServiceAsync mediaService = GWT.create(MediaArchiveService.class);
    
    private final Messages messages = GWT.create(Messages.class);
    
    TextBox nameField = new TextBox();
    TextBox titleField = new TextBox();
    TextBox dateField = new TextBox();
    TextBox eventField = new TextBox();
    TextBox descriptionField = new TextBox();
    
    Label postLabel = new Label(messages.postLabel());
    ListBox postList = new ListBox();
    Label memberLabel = new Label(messages.memberLabel());
    ListBox memberList = new ListBox();
    Button searchButton = new Button(messages.searchButtonLabel());
    Button cancelButton = new Button(messages.cancelButtonLabel());
    
    public SearchDialogBox() {
        postList.addItem("-");
        memberList.addItem("-");
        mediaService.getMembers(new AsyncCallback<List<BSSMember>>() {
            @Override
            public void onSuccess(List<BSSMember> result) {
                for (BSSMember bssMember : result) {
                    memberList.addItem(bssMember.getListName());
                }
            }
            
            @Override
            public void onFailure(Throwable caught) {}
        });
        
        setText("Keresés");
        setAnimationEnabled(true);
        setGlassEnabled(true);
        
        searchButton.addClickHandler(new SearchEntityHandler());
        
        cancelButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                SearchDialogBox.this.hide();
            }
        });
        
        VerticalPanel panel = new VerticalPanel();
        panel.setHeight("100");
        panel.setWidth("300");
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        
        BSSMedia.addPostsToListBox(postList);
        
        panel.add(new Label("Név: "));
        panel.add(nameField);
        panel.add(new Label("Cím: "));
        panel.add(titleField);
        panel.add(new Grid());
        panel.add(new Label("Dátum (\"ÉÉÉÉ-HH-NN\" formátum, \"**\" [minden lehetőség] megengedett): "));
        panel.add(dateField);
        panel.add(new Grid());
        panel.add(new Label("Esemény: "));
        panel.add(eventField);
        panel.add(new Grid());
        panel.add(new Label("Leírás: "));
        panel.add(descriptionField);
        panel.add(new Grid());
        panel.add(postLabel);
        panel.add(postList);
        panel.add(new Grid());
        panel.add(memberLabel);
        panel.add(memberList);
        panel.add(new Grid());
        panel.add(searchButton);
        panel.add(new Grid());
        panel.add(cancelButton);
        
        setWidget(panel);
    }
    
    class SearchEntityHandler implements ClickHandler {
        
        @Override
        public void onClick(ClickEvent event) {
            List<String> searchValues = new ArrayList<>();
            searchValues.add(nameField.getText());
            searchValues.add(titleField.getText());
            searchValues.add(dateField.getText());
            searchValues.add(eventField.getText());
            searchValues.add(descriptionField.getText());
            if (postList.getSelectedValue().equals("-")) {
                searchValues.add(new String());
            } else {
                searchValues.add(postList.getSelectedValue());
            }
            if (memberList.getSelectedValue().equals("-")) {
                searchValues.add(new String());
            } else {
                searchValues.add(memberList.getSelectedValue());
            }
            mediaService.searchEntity(searchValues, new AsyncCallback<List<MediaEntity>>() {
                @Override
                public void onSuccess(List<MediaEntity> result) {
                    showResult(result);
                    hide();
                }
                @Override
                public void onFailure(Throwable caught) {}
            });
        }
        
        private void showResult(List<MediaEntity> result) {
            final DialogBox resultDialog = new DialogBox();
            resultDialog.setText("Találatok");
            resultDialog.setAnimationEnabled(true);
            resultDialog.setGlassEnabled(true);
            
            Button closeButton = new Button("OK");
            closeButton.addClickHandler(new ClickHandler() {
               public void onClick(ClickEvent event) {
                   resultDialog.hide();
               }
            });
            
            VerticalPanel panel = new VerticalPanel();
            panel.setHeight("100");
            panel.setWidth("300");
            panel.setSpacing(10);
            panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
            
            CellTable<MediaEntity> resultTable = new CellTable<>();
            TextColumn<MediaEntity> videoNameColumn = new TextColumn<MediaEntity>() {
                @Override
                public String getValue(MediaEntity entity) {
                    return entity.getName();
                }
            };
            TextColumn<MediaEntity> videoTitleColumn = new TextColumn<MediaEntity>() {
                @Override
                public String getValue(MediaEntity entity) {
                    return entity.getTitle();
                }
            };
            TextColumn<MediaEntity> ltoNameColumn = new TextColumn<MediaEntity>() {
                @Override
                public String getValue(MediaEntity entity) {
                    return entity.getLtoName();
                }
            };
            TextColumn<MediaEntity> folderNameColumn = new TextColumn<MediaEntity>() {
                @Override
                public String getValue(MediaEntity entity) {
                    return entity.getFolderName();
                }
            };
            resultTable.addColumn(videoNameColumn, "Video neve");
            resultTable.addColumn(videoTitleColumn, "Video címe");
            resultTable.addColumn(ltoNameColumn, "LTO kazetta címe");
            resultTable.addColumn(folderNameColumn, "Mappa neve");
            resultTable.setWidth("100%", true);
            resultTable.setColumnWidth(videoNameColumn, 30.0, Unit.PCT);
            resultTable.setColumnWidth(videoTitleColumn, 30.0, Unit.PCT);
            resultTable.setColumnWidth(ltoNameColumn, 20.0, Unit.PCT);
            resultTable.setColumnWidth(ltoNameColumn, 20.0, Unit.PCT);
            resultTable.setRowData(result);
            
            panel.add(resultTable);
            panel.add(closeButton);
            
            resultDialog.setWidget(panel);
            resultDialog.setPopupPosition(getAbsoluteLeft(), getAbsoluteTop());
            resultDialog.show();
        }
    }
    
}
