package com.bss.media.client.ui;


public class VideoTableData {
    
    private String videoName;
    private String date;
    private String videoSize;
    
    public VideoTableData(String videoName, String date, String videoSize) {
        this.videoName = videoName;
        this.date = date;
        this.videoSize = videoSize;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoSize() {
        return videoSize;
    }

    public void setVideoSize(String videoSize) {
        this.videoSize = videoSize;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
