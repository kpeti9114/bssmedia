package com.bss.media.client.ui;

import com.bss.media.client.MediaArchiveService;
import com.bss.media.client.MediaArchiveServiceAsync;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AdminDialogBox extends DialogBox {
    
    private final MediaArchiveServiceAsync mediaService = GWT.create(MediaArchiveService.class);
    
    public AdminDialogBox() {
        
        final TextBox ltoName = new TextBox();
        final TextBox ltoSize = new TextBox();
        
        setText("Adminisztrátori panel");
        setAnimationEnabled(true);
        setGlassEnabled(true);
        
        VerticalPanel panel = new VerticalPanel();
        panel.setHeight("100");
        panel.setWidth("300");
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        
        panel.add(new Grid());
        panel.add(new Label("LTO szalag neve: "));
        panel.add(ltoName);
        panel.add(new Grid());
        panel.add(new Label("LTO szalag mérete: "));
        panel.add(ltoSize);
        panel.add(new Grid());
        
        Button closeButton = new Button("Mégsem");
        closeButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                AdminDialogBox.this.hide();
            }
        });
        
        Button updateButton = new Button("Módosít");
        updateButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                mediaService.getLTOInfo(new AsyncCallback<LTOTapeEntity>() {
                    
                    @Override
                    public void onSuccess(LTOTapeEntity result) {
                        result.setLtoName(ltoName.getText());
                        result.setFreeSpace(Long.valueOf(ltoSize.getText()));
                        mediaService.updateLTOInfo(result, new AsyncCallback<Void>() {
                            
                            @Override
                            public void onSuccess(Void result) {
                                Window.alert("A módosítás sikerült!");
                                AdminDialogBox.this.hide();
                            }
                            @Override
                            public void onFailure(Throwable caught) {}
                        });
                    }
                    @Override
                    public void onFailure(Throwable caught) {}
                });
            }
        });
        Button setArchivingEnableButton = new Button("Archiválás engedélyezése");
        setArchivingEnableButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mediaService.setLTOWritableInfo(true, new AsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        AdminDialogBox.this.hide();
                    }
                    @Override
                    public void onFailure(Throwable caught) {}
                });
            }
        });
        Button setArchivingDisableButton = new Button("Archiválás tiltása");
        setArchivingDisableButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mediaService.setLTOWritableInfo(false, new AsyncCallback<Void>() {
                    @Override
                    public void onSuccess(Void result) {
                        AdminDialogBox.this.hide();
                    }
                    @Override
                    public void onFailure(Throwable caught) {}
                });
            }
        });
        
        panel.add(updateButton);
        panel.add(new Grid());
        panel.add(setArchivingEnableButton);
        panel.add(setArchivingDisableButton);
        panel.add(new Grid());
        panel.add(closeButton);
        
        setWidget(panel);
    }
}
