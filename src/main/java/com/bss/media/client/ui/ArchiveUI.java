package com.bss.media.client.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bss.media.client.BSSMedia;
import com.bss.media.client.MediaArchiveService;
import com.bss.media.client.MediaArchiveServiceAsync;
import com.bss.media.client.Messages;
import com.bss.media.shared.FieldVerifier;
import com.bss.media.shared.entity.BSSMember;
import com.bss.media.shared.entity.BSSPost;
import com.bss.media.shared.entity.LTOTapeEntity;
import com.bss.media.shared.entity.MediaEntity;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class ArchiveUI {
    
    private final MediaArchiveServiceAsync mediaService = GWT.create(MediaArchiveService.class);
    
    private final Messages messages = GWT.create(Messages.class);
    
    private Map<BSSPost, ArrayList<String>> currentStaff = new HashMap<BSSPost, ArrayList<String>>();
    private List<StaffTableData> staffTableDataList = new ArrayList<>();
    private List<VideoTableData> videoTableDataList = new ArrayList<>();
    
    final CellTable<VideoTableData> videos = new CellTable<>();
    final Label chooseLabel = new Label(messages.chooseLabel());
    final ListBox videoChooseList = new ListBox();
    
    final Label titleLabel = new Label(messages.titleLabel());
    final TextBox titleField = new TextBox();
    final Label dateLabel = new Label(messages.dateLabel());
    final TextBox dateField = new TextBox();
    final Label eventLabel = new Label(messages.eventLabel());
    final TextBox eventField = new TextBox();
    final Label descriptionLabel = new Label(messages.descriptionLabel());
    final TextBox descriptionField = new TextBox();
    
    final Label postLabel = new Label(messages.postLabel());
    final ListBox postList = new ListBox();
    final Label memberLabel = new Label(messages.memberLabel());
    final ListBox memberList = new ListBox();
    final Button addMemberButton = new Button(messages.addMemberButtonLabel());
    
    final CellTable<StaffTableData> staffTable = new CellTable<>();
    final ButtonCell deleteButtonCell = new ButtonCell();
    
    final Label selectLabel = new Label(messages.selectLabel());
    final RadioButton noRadioButton = new RadioButton("radioGroup", "Nem");
    final RadioButton yesRadioButton = new RadioButton("radioGroup", "Igen");
    
    final Label requiredLabel = new Label(messages.requiredLabel());
    
    final Button archiveButton = new Button(messages.archiveButtonLabel());
    
    final Button searchButton = new Button(messages.searchButtonLabel());
    
    LTOTapeEntity ltoTape;
    
    @SuppressWarnings("unchecked")
    public ArchiveUI() {
        mediaService.isLTOWritable(new AsyncCallback<Boolean>() {
            
            @Override
            public void onSuccess(Boolean result) {
                archiveButton.setVisible(result);
            }
            @Override
            public void onFailure(Throwable caught) {}
        });
        
        mediaService.getVideos(new AsyncCallback<List<String>>() {
            
            @Override
            public void onSuccess(List<String> result) {
                int i = 0;
                while(i < result.size()) {
                    videoTableDataList.add(new VideoTableData(result.get(i), result.get(i+1), 
                        result.get(i+2)));
                    videoChooseList.addItem(result.get(i));
                    i += 3;
                }
                videos.setRowData(videoTableDataList);
            }
            @Override
            public void onFailure(Throwable caught) {}
        });
        
        BSSMedia.addPostsToListBox(postList);
        
        TextColumn<VideoTableData> videoNameColumn = new TextColumn<VideoTableData>() {
            @Override
            public String getValue(VideoTableData data) {
                return data.getVideoName();
            }
        };
        TextColumn<VideoTableData> videoDateColumn = new TextColumn<VideoTableData>() {
            @Override
            public String getValue(VideoTableData data) {
                return data.getDate();
            }
        };
        TextColumn<VideoTableData> videoSizeColumn = new TextColumn<VideoTableData>() {
            @Override
            public String getValue(VideoTableData data) {
                return data.getVideoSize();
            }
        };
        
        videos.addColumn(videoNameColumn, "Videó neve");
        videos.addColumn(videoDateColumn, "Dátum");
        videos.addColumn(videoSizeColumn, "Videó mérete");
        videos.setWidth("100%", true);
        videos.setColumnWidth(videoNameColumn, 60.0, Unit.PCT);
        videos.setColumnWidth(videoDateColumn, 20.0, Unit.PCT);
        videos.setColumnWidth(videoSizeColumn, 20.0, Unit.PCT);

        staffTable.setRowData(new ArrayList<StaffTableData>());
        
        addMemberButton.addClickHandler(new AddMemberHandler());
        
        archiveButton.addClickHandler(new ArchiveHandler());
        
        searchButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                SearchDialogBox searchDialogBox = new SearchDialogBox();
                searchDialogBox.center();
                searchDialogBox.show();
            }
        });
        
        noRadioButton.setValue(true);
        
        mediaService.getMembers(new AsyncCallback<List<BSSMember>>() {
            @Override
            public void onSuccess(List<BSSMember> result) {
                for (BSSMember bssMember : result) {
                    memberList.addItem(bssMember.getListName());
                }
            }
            
            @Override
            public void onFailure(Throwable caught) {}
        });
        
        TextColumn<StaffTableData> postColumn = new TextColumn<StaffTableData>() {
            @Override
            public String getValue(StaffTableData data) {
                return data.getBssPost().toString();
            }
        };
        TextColumn<StaffTableData> nameColumn = new TextColumn<StaffTableData>() {
            
            @Override
            public String getValue(StaffTableData data) {
                return data.getMemberNames();
            }
        };
        @SuppressWarnings("rawtypes")
        Column buttonColumn = new Column<StaffTableData, String>(deleteButtonCell) {
            @Override
            public String getValue(StaffTableData object) {
                return "Törlés";
            }
        };
        buttonColumn.setFieldUpdater(new FieldUpdater<StaffTableData, String>() {
            @Override
            public void update(int index, StaffTableData staffTableData, String value) {
                currentStaff.remove(staffTableData.getBssPost());
                staffTableDataList.remove(staffTableData);
                staffTable.setRowData(staffTableDataList);
                staffTable.flush();
            }
        });
        
        staffTable.addColumn(postColumn, "Poszt");
        staffTable.addColumn(nameColumn, "Személy");
        staffTable.addColumn(buttonColumn);
        staffTable.setWidth("100%", true);
        staffTable.setColumnWidth(postColumn, 40.0, Unit.PCT);
        staffTable.setColumnWidth(nameColumn, 40.0, Unit.PCT);
    }
    
    private void archiveVideo() {
        if (!validateInput()) {
            return;
        }
        mediaService.getLTOInfo(new AsyncCallback<LTOTapeEntity>() {
            @Override
            public void onSuccess(LTOTapeEntity result) {
                ltoTape = result;
                if (!isEnoughFreeSpace()) {
                    return;
                }
                final MediaEntity mediaEntity = new MediaEntity();
                mediaEntity.setTitle(titleField.getText().toLowerCase());
                mediaEntity.setDate(DateTimeFormat.getFormat("yyyy-MM-dd").parse(dateField.getText()));
                mediaEntity.setEvent(eventField.getText());
                mediaEntity.setDescription(descriptionField.getText());
                mediaEntity.setStaff(currentStaff);
                if (noRadioButton.getValue()) {
                    mediaEntity.setDeletable(false);
                } else {
                    mediaEntity.setDeletable(true);
                }
                mediaEntity.setLtoName(ltoTape.getLtoName());
                Date savingDate = new Date();
                mediaEntity.setFolderName(savingDate.getYear() + "-" + savingDate.getMonth());

                mediaEntity.setName(videoChooseList.getSelectedItemText());
                
                mediaService.saveMediaEntity(mediaEntity, new AsyncCallback<Long>() {
                    @Override
                    public void onSuccess(Long id) {
                        showSavedEntityProperties(id);
                        titleField.setText("");
                        dateField.setText("");
                        eventField.setText("");
                        descriptionField.setText("");
                        currentStaff.clear();
                        staffTableDataList.clear();
                        mediaService.setLTOWritableInfo(false, new AsyncCallback<Void>() {
                            @Override
                            public void onSuccess(Void result) {
                                mediaService.sendEmailToAdministrator(mediaEntity, new AsyncCallback<Void>() {
                                    @Override
                                    public void onSuccess(Void result) {}
                                    @Override
                                    public void onFailure(Throwable caught) {}
                                });
                            }
                            @Override
                            public void onFailure(Throwable caught) {}
                        });
                    }
                    @Override
                    public void onFailure(Throwable caught) {}
                });
            }
            @Override
            public void onFailure(Throwable caught) {}
        });
        
    }
    
    public void showSavedEntityProperties(Long id) {
        mediaService.getEntity(id, new AsyncCallback<MediaEntity>() {
            @Override
            public void onSuccess(MediaEntity result) {
                SavedDialogBox dialogBox = new SavedDialogBox(result);
                dialogBox.center();
                dialogBox.show();
            }
            @Override
            public void onFailure(Throwable caught) {}
        });
    }
    
    public List<Widget> getElements() {
        List<Widget> elements = new ArrayList<>();
        elements.add(videos);
        elements.add(chooseLabel);
        elements.add(videoChooseList);
        
        elements.add(titleLabel);
        elements.add(titleField);
        
        elements.add(dateLabel);
        elements.add(dateField);
        
        elements.add(eventLabel);
        elements.add(eventField);
        
        elements.add(descriptionLabel);
        elements.add(descriptionField);
        
        elements.add(postLabel);
        elements.add(postList);
        elements.add(memberLabel);
        elements.add(memberList);
        elements.add(addMemberButton);
        elements.add(staffTable);
        
        elements.add(selectLabel);
        elements.add(noRadioButton);
        elements.add(yesRadioButton);
        
        elements.add(requiredLabel);
        
        elements.add(archiveButton);
        
        elements.add(searchButton);
        
        return elements;
    }
    
    class AddMemberHandler implements ClickHandler {
        
        @Override
        public void onClick(ClickEvent event) {
            BSSPost selectedPost = BSSPost.valueOf(postList.getSelectedItemText());
            if (currentStaff.containsKey(selectedPost)) {
                currentStaff.get(selectedPost).add(memberList.getSelectedValue());
            } else {
                ArrayList<String> members = new ArrayList<>();
                members.add(memberList.getSelectedItemText());
                currentStaff.put(selectedPost, members);
            }
            boolean addNewRow = true;
            for (StaffTableData staffTableData : staffTableDataList) {
                if (staffTableData.getBssPost().equals(selectedPost)) {
                    staffTableData.setMembers(currentStaff.get(selectedPost));
                    addNewRow = false;
                }
            }
            if (addNewRow) {
                staffTableDataList.add(new StaffTableData(selectedPost, currentStaff.get(selectedPost)));
            }
            staffTable.setRowData(staffTableDataList);
            staffTable.flush();
        }
    }
    
    class ArchiveHandler implements ClickHandler {
        
        @Override
        public void onClick(ClickEvent event) {
            archiveVideo();
        }
    }
    
    private boolean validateInput() {
        if (!FieldVerifier.isFilledText(titleField.getText())) {
            Window.alert("A \"cím\" mező kitöltése kötelező!");
            return false;
        } else if (!FieldVerifier.isFilledText(dateField.getText())) {
            Window.alert("A \"dátum\" mező kitöltése kötelező!");
            return false;
        } else if (!FieldVerifier.isValidDate(dateField.getText())) {
            Window.alert("Helytelen dátumformátum!");
            return false;
        } else if (!FieldVerifier.isFilledText(eventField.getText())) {
            Window.alert("Az \"esemény neve\" mező kitöltése kötelező!");
            return false;
        }
        return true;
    }
    
    private boolean isEnoughFreeSpace() {
        long freespace = ltoTape.getFreeSpace();
        for (VideoTableData data : videoTableDataList) {
            if (data.getVideoName().equals(videoChooseList.getSelectedItemText())) {
                freespace = freespace - 1;
//                    Long.parseLong(data.getVideoSize().substring(0, data.getVideoSize().length() - 1));
            }
        }
        if (freespace >= 0) {
            ltoTape.setFreeSpace(freespace);
            mediaService.updateLTOInfo(ltoTape, new AsyncCallback<Void>() {
                @Override
                public void onSuccess(Void result) {
                    archiveButton.setVisible(false);
                }
                @Override
                public void onFailure(Throwable caught) {}
            });
            return true;
        } else {
            Window.alert("Nincs elég hely az LTO szalagon! Miután az adminisztrátor kicserélte a kazettát"
                + " próbálja meg újra!");
            archiveButton.setVisible(false);
            return false;
        }
    }
    
}
