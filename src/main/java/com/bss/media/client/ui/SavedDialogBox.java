package com.bss.media.client.ui;

import com.bss.media.shared.entity.MediaEntity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SavedDialogBox extends DialogBox {
    
    public SavedDialogBox(MediaEntity mediaEntity) {
        
        setText("Entitás elmentve");
        setAnimationEnabled(true);
        setGlassEnabled(true);
        
        Button closeButton = new Button("OK");
        closeButton.addClickHandler(new ClickHandler() {
           public void onClick(ClickEvent event) {
               SavedDialogBox.this.hide();
           }
        });
        
        VerticalPanel panel = new VerticalPanel();
        panel.setHeight("100");
        panel.setWidth("300");
        panel.setSpacing(10);
        panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        
        panel.add(new Grid());
        panel.add(new Label("LTO neve: "));
        panel.add(new Label(mediaEntity.getLtoName()));
        panel.add(new Grid());
        panel.add(new Label("Mappa neve: "));
        panel.add(new Label(mediaEntity.getFolderName()));
        panel.add(new Grid());
        panel.add(new Label("Video neve: "));
        panel.add(new Label(mediaEntity.getName()));
        panel.add(closeButton);
        
        setWidget(panel);
    }
}