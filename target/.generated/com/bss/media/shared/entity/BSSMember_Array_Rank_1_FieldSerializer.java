package com.bss.media.shared.entity;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BSSMember_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.bss.media.shared.entity.BSSMember[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.bss.media.shared.entity.BSSMember[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new com.bss.media.shared.entity.BSSMember[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.bss.media.shared.entity.BSSMember[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.bss.media.shared.entity.BSSMember_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSMember_Array_Rank_1_FieldSerializer.deserialize(reader, (com.bss.media.shared.entity.BSSMember[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSMember_Array_Rank_1_FieldSerializer.serialize(writer, (com.bss.media.shared.entity.BSSMember[])object);
  }
  
}
