package com.bss.media.shared.entity;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MediaEntity_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getDate(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::date;
  }-*/;
  
  private static native void setDate(com.bss.media.shared.entity.MediaEntity instance, java.util.Date value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::date = value;
  }-*/;
  
  private static native java.lang.String getDescription(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::description;
  }-*/;
  
  private static native void setDescription(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::description = value;
  }-*/;
  
  private static native java.lang.String getEvent(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::event;
  }-*/;
  
  private static native void setEvent(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::event = value;
  }-*/;
  
  private static native java.lang.String getFolderName(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::folderName;
  }-*/;
  
  private static native void setFolderName(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::folderName = value;
  }-*/;
  
  private static native java.lang.Long getId(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::id;
  }-*/;
  
  private static native void setId(com.bss.media.shared.entity.MediaEntity instance, java.lang.Long value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::id = value;
  }-*/;
  
  private static native boolean getIsDeletable(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::isDeletable;
  }-*/;
  
  private static native void setIsDeletable(com.bss.media.shared.entity.MediaEntity instance, boolean value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::isDeletable = value;
  }-*/;
  
  private static native java.lang.String getLtoName(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::ltoName;
  }-*/;
  
  private static native void setLtoName(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::ltoName = value;
  }-*/;
  
  private static native java.lang.String getName(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::name;
  }-*/;
  
  private static native void setName(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::name = value;
  }-*/;
  
  private static native java.util.Map getStaff(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::staff;
  }-*/;
  
  private static native void setStaff(com.bss.media.shared.entity.MediaEntity instance, java.util.Map value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::staff = value;
  }-*/;
  
  private static native java.lang.String getTitle(com.bss.media.shared.entity.MediaEntity instance) /*-{
    return instance.@com.bss.media.shared.entity.MediaEntity::title;
  }-*/;
  
  private static native void setTitle(com.bss.media.shared.entity.MediaEntity instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.MediaEntity::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.bss.media.shared.entity.MediaEntity instance) throws SerializationException {
    setDate(instance, (java.util.Date) streamReader.readObject());
    setDescription(instance, streamReader.readString());
    setEvent(instance, streamReader.readString());
    setFolderName(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setIsDeletable(instance, streamReader.readBoolean());
    setLtoName(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setStaff(instance, (java.util.Map) streamReader.readObject());
    setTitle(instance, streamReader.readString());
    
  }
  
  public static com.bss.media.shared.entity.MediaEntity instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.bss.media.shared.entity.MediaEntity();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.bss.media.shared.entity.MediaEntity instance) throws SerializationException {
    streamWriter.writeObject(getDate(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getEvent(instance));
    streamWriter.writeString(getFolderName(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeBoolean(getIsDeletable(instance));
    streamWriter.writeString(getLtoName(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getStaff(instance));
    streamWriter.writeString(getTitle(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.bss.media.shared.entity.MediaEntity_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.bss.media.shared.entity.MediaEntity_FieldSerializer.deserialize(reader, (com.bss.media.shared.entity.MediaEntity)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.bss.media.shared.entity.MediaEntity_FieldSerializer.serialize(writer, (com.bss.media.shared.entity.MediaEntity)object);
  }
  
}
