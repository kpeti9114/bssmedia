package com.bss.media.shared.entity;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BSSMember_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getActualPost(com.bss.media.shared.entity.BSSMember instance) /*-{
    return instance.@com.bss.media.shared.entity.BSSMember::actualPost;
  }-*/;
  
  private static native void setActualPost(com.bss.media.shared.entity.BSSMember instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.BSSMember::actualPost = value;
  }-*/;
  
  private static native java.lang.Long getId(com.bss.media.shared.entity.BSSMember instance) /*-{
    return instance.@com.bss.media.shared.entity.BSSMember::id;
  }-*/;
  
  private static native void setId(com.bss.media.shared.entity.BSSMember instance, java.lang.Long value) 
  /*-{
    instance.@com.bss.media.shared.entity.BSSMember::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.bss.media.shared.entity.BSSMember instance) /*-{
    return instance.@com.bss.media.shared.entity.BSSMember::name;
  }-*/;
  
  private static native void setName(com.bss.media.shared.entity.BSSMember instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.BSSMember::name = value;
  }-*/;
  
  private static native java.lang.String getNickname(com.bss.media.shared.entity.BSSMember instance) /*-{
    return instance.@com.bss.media.shared.entity.BSSMember::nickname;
  }-*/;
  
  private static native void setNickname(com.bss.media.shared.entity.BSSMember instance, java.lang.String value) 
  /*-{
    instance.@com.bss.media.shared.entity.BSSMember::nickname = value;
  }-*/;
  
  private static native com.bss.media.shared.entity.BSSStatus getStatus(com.bss.media.shared.entity.BSSMember instance) /*-{
    return instance.@com.bss.media.shared.entity.BSSMember::status;
  }-*/;
  
  private static native void setStatus(com.bss.media.shared.entity.BSSMember instance, com.bss.media.shared.entity.BSSStatus value) 
  /*-{
    instance.@com.bss.media.shared.entity.BSSMember::status = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.bss.media.shared.entity.BSSMember instance) throws SerializationException {
    setActualPost(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setName(instance, streamReader.readString());
    setNickname(instance, streamReader.readString());
    setStatus(instance, (com.bss.media.shared.entity.BSSStatus) streamReader.readObject());
    
  }
  
  public static com.bss.media.shared.entity.BSSMember instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.bss.media.shared.entity.BSSMember();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.bss.media.shared.entity.BSSMember instance) throws SerializationException {
    streamWriter.writeString(getActualPost(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getNickname(instance));
    streamWriter.writeObject(getStatus(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.bss.media.shared.entity.BSSMember_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSMember_FieldSerializer.deserialize(reader, (com.bss.media.shared.entity.BSSMember)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSMember_FieldSerializer.serialize(writer, (com.bss.media.shared.entity.BSSMember)object);
  }
  
}
