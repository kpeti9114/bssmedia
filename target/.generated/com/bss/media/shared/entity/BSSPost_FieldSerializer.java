package com.bss.media.shared.entity;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BSSPost_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.bss.media.shared.entity.BSSPost instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static com.bss.media.shared.entity.BSSPost instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    com.bss.media.shared.entity.BSSPost[] values = com.bss.media.shared.entity.BSSPost.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.bss.media.shared.entity.BSSPost instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.bss.media.shared.entity.BSSPost_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSPost_FieldSerializer.deserialize(reader, (com.bss.media.shared.entity.BSSPost)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.bss.media.shared.entity.BSSPost_FieldSerializer.serialize(writer, (com.bss.media.shared.entity.BSSPost)object);
  }
  
}
