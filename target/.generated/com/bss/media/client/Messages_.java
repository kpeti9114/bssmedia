package com.bss.media.client;

public class Messages_ implements com.bss.media.client.Messages {
  
  public java.lang.String chooseLabel() {
    return "Válassza ki az archiválandó videót a listábó: ";
  }
  
  public java.lang.String titleLabel() {
    return "* Adja meg a video címét: ";
  }
  
  public java.lang.String dateLabel() {
    return "* Adja meg a dátumot \"ÉÉÉÉ-HH-NN\" alakban: ";
  }
  
  public java.lang.String eventLabel() {
    return "* Adja meg az esemény nevét, amihez a video kapcsolódik (csak 1 szó): ";
  }
  
  public java.lang.String descriptionLabel() {
    return "Adjon meg a leírást: ";
  }
  
  public java.lang.String postLabel() {
    return "Válasszon posztot: ";
  }
  
  public java.lang.String memberLabel() {
    return "Válasszon tagot az adott posztra: ";
  }
  
  public java.lang.String addMemberButtonLabel() {
    return "Hozzáadás";
  }
  
  public java.lang.String selectLabel() {
    return "* Automatikusan törölhető?";
  }
  
  public java.lang.String archiveButtonLabel() {
    return "Archiválás";
  }
  
  public java.lang.String requiredLabel() {
    return "A *-al jelölt mezők kitöltése kötelező!";
  }
  
  public java.lang.String searchButtonLabel() {
    return "Keresés";
  }
  
  public java.lang.String cancelButtonLabel() {
    return "Mégsem";
  }
}
