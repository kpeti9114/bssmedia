package com.bss.media.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MediaArchiveServiceAsync
{

    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void createInitData( AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void authenticateUser( java.lang.String userName, java.lang.String password, AsyncCallback<java.lang.Boolean> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void isUserAdmin( java.lang.String userName, java.lang.String password, AsyncCallback<java.lang.Boolean> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void saveMediaEntity( com.bss.media.shared.entity.MediaEntity entity, AsyncCallback<java.lang.Long> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void getEntity( java.lang.Long id, AsyncCallback<com.bss.media.shared.entity.MediaEntity> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void searchEntity( java.util.List<java.lang.String> searchValues, AsyncCallback<java.util.List<com.bss.media.shared.entity.MediaEntity>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void getMembers( AsyncCallback<java.util.List<com.bss.media.shared.entity.BSSMember>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void getVideos( AsyncCallback<java.util.List<java.lang.String>> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void getLTOInfo( AsyncCallback<com.bss.media.shared.entity.LTOTapeEntity> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void updateLTOInfo( com.bss.media.shared.entity.LTOTapeEntity entity, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void isLTOWritable( AsyncCallback<java.lang.Boolean> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void setLTOWritableInfo( boolean isWritable, AsyncCallback<Void> callback );


    /**
     * GWT-RPC service  asynchronous (client-side) interface
     * @see com.bss.media.client.MediaArchiveService
     */
    void sendEmailToAdministrator( com.bss.media.shared.entity.MediaEntity entity, AsyncCallback<Void> callback );


    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static MediaArchiveServiceAsync instance;

        public static final MediaArchiveServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (MediaArchiveServiceAsync) GWT.create( MediaArchiveService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instantiated
        }
    }
}
